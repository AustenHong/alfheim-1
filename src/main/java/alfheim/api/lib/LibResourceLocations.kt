package alfheim.api.lib

import alfheim.api.ModInfo
import net.minecraft.util.ResourceLocation
import java.util.*

object LibResourceLocations {
	
	val akashicBox = ResourceLocation(ModInfo.MODID, "textures/model/item/AkashicBox.png")
	val akashicCube = ResourceLocation(ModInfo.MODID, "textures/model/item/AkashicRecordsCube.png")
	val akashicCube_ = ResourceLocation(ModInfo.MODID, "textures/model/item/AkashicRecordsCube_noShader.png")
	val akashicCyl = ResourceLocation(ModInfo.MODID, "textures/model/item/AkashicRecordsCyl.png")
	val altar9 = ResourceLocation(ModInfo.MODID, "textures/model/block/altar9.png")
	val anomalies = ResourceLocation(ModInfo.MODID, "textures/misc/anomalies.png")
	val antiPylon = ResourceLocation(ModInfo.MODID, "textures/model/block/AntiPylon.png")
	val antiPylonOld = ResourceLocation(ModInfo.MODID, "textures/model/block/AntiPylonOld.png")
	val anyavil = ResourceLocation(ModInfo.MODID, "textures/model/block/Anyavil.png")
	var babylon = ResourceLocation("botania:textures/misc/babylon.png")
	val balanceCloak = ResourceLocation(ModInfo.MODID, "textures/model/armor/BalanceCloak.png")
	val butterfly = ResourceLocation(ModInfo.MODID, "textures/misc/Butterfly.png")
	val cross = ResourceLocation(ModInfo.MODID, "textures/misc/crosshair.png")
	val deathTimer = ResourceLocation(ModInfo.MODID, "textures/gui/DeathTimer.png")
	val deathTimerBG = ResourceLocation(ModInfo.MODID, "textures/gui/DeathTimerBack.png")
	val elementiumBlock = ResourceLocation("botania:textures/blocks/storage2.png")
	val elf = ResourceLocation(ModInfo.MODID, "textures/model/entity/Elf.png")
	var elvenPylon = ResourceLocation("botania:textures/model/pylon2.png")
	var elvenPylonOld = ResourceLocation("botania:textures/model/pylonOld2.png")
	val elvoriumArmor = ResourceLocation(ModInfo.MODID, "textures/model/armor/ElvoriumArmor.png")
	val explosion = ResourceLocation(ModInfo.MODID, "textures/misc/explosion.png")
	val gaiaPylon = ResourceLocation(ModInfo.MODID, "textures/model/block/GaiaPylon.png")
	val gaiaPylonOld = ResourceLocation(ModInfo.MODID, "textures/model/block/GaiaPylonOld.png")
	val glow = ResourceLocation(ModInfo.MODID, "textures/misc/glow.png")
	var glowCyan = ResourceLocation("botania:textures/misc/glow1.png")
	val gravel = ResourceLocation(ModInfo.MODID, "textures/misc/gravel.png")
	val gravity = ResourceLocation(ModInfo.MODID, "textures/model/entity/Gravity.png")
	var halo = ResourceLocation("botania:textures/misc/halo.png")
	val harp = ResourceLocation(ModInfo.MODID, "textures/model/entity/harp.png")
	val health = ResourceLocation(ModInfo.MODID, "textures/gui/health.png")
	val hotSpells = ResourceLocation(ModInfo.MODID, "textures/gui/HotSpells.png")
	val iceLens = ResourceLocation(ModInfo.MODID, "textures/misc/IceLens.png")
	val jibril = ResourceLocation(ModInfo.MODID, "textures/model/entity/Jibril.png")
	val jibrilDark = ResourceLocation(ModInfo.MODID, "textures/model/entity/JibrilDark.png")
	var lexica = ResourceLocation("botania:textures/model/lexica.png")
	val livingrock = ResourceLocation("botania:textures/blocks/livingrock0.png")
	val lolicorn = ResourceLocation(ModInfo.MODID, "textures/model/entity/Lolicorn.png")
	var manaInfuserOverlay = ResourceLocation("botania:textures/gui/manaInfusionOverlay.png")
	val mark = ResourceLocation(ModInfo.MODID, "textures/model/entity/Mark.png")
	val miko1 = ResourceLocation(ModInfo.MODID, "textures/model/entity/Miko1.png")
	val miko2 = ResourceLocation(ModInfo.MODID, "textures/model/entity/Miko2.png")
	val miku0 = ResourceLocation(ModInfo.MODID, "textures/model/entity/Miku0.png")
	val miku1 = ResourceLocation(ModInfo.MODID, "textures/model/entity/Miku1.png")
	val miku2 = ResourceLocation(ModInfo.MODID, "textures/model/entity/Miku2.png")
	val mine1 = ResourceLocation(ModInfo.MODID, "textures/model/entity/1.png")
	val mine2 = ResourceLocation(ModInfo.MODID, "textures/model/entity/2.png")
	val mine3 = ResourceLocation(ModInfo.MODID, "textures/model/entity/3.png")
	val palette = ResourceLocation(ModInfo.MODID, "textures/misc/pal.png")
	var petalOverlay = ResourceLocation("botania:textures/gui/petalOverlay.png")
	var pixie = ResourceLocation("botania:textures/model/pixie.png")
	val poolBlue = ResourceLocation(ModInfo.MODID, "textures/blocks/PoolBlue.png")
	val poolPink = ResourceLocation(ModInfo.MODID, "textures/blocks/PoolPink.png")
	val potions = ResourceLocation(ModInfo.MODID, "textures/gui/Potions.png")
	val rook = ResourceLocation(ModInfo.MODID, "textures/model/entity/Rook.png")
	val roricorn = ResourceLocation(ModInfo.MODID, "textures/model/entity/Roricorn.png")
	val shavermik = ResourceLocation(ModInfo.MODID, "textures/model/entity/shavermik.png")
	val skin = ResourceLocation(ModInfo.MODID, "textures/model/entity/AlexSocol.png")
	val spellFrame = ResourceLocation(ModInfo.MODID, "textures/gui/spellframe.png")
	val spellFrameEpic = ResourceLocation(ModInfo.MODID, "textures/gui/spellframeepic.png")
	val spellRace = ResourceLocation(ModInfo.MODID, "textures/gui/SpellRace.png")
	var spreader = ResourceLocation("botania:textures/model/spreader.png")
	val target = ResourceLocation(ModInfo.MODID, "textures/misc/target.png")
	val targetq = ResourceLocation(ModInfo.MODID, "textures/misc/targetq.png")
	val uberSpreader = ResourceLocation(ModInfo.MODID, "textures/model/block/uberSpreader.png")
	val uberSpreaderFrame = ResourceLocation(ModInfo.MODID, "textures/model/block/uberSpreaderFrame.png")
	val uberSpreaderGolden = ResourceLocation(ModInfo.MODID, "textures/model/block/uberSpreaderGolden.png")
	val uberSpreaderHalloween = ResourceLocation(ModInfo.MODID, "textures/model/block/uberSpreaderHalloween.png")
	val uberSpreaderHalloweenGolden = ResourceLocation(ModInfo.MODID, "textures/model/block/uberSpreaderHalloweenGolden.png")
	val wind = ResourceLocation(ModInfo.MODID, "textures/model/entity/wind.png")
	val yordinPylon = ResourceLocation(ModInfo.MODID, "textures/model/block/ElvenPylon.png")
	val yordinPylonOld = ResourceLocation(ModInfo.MODID, "textures/model/block/ElvenPylonOld.png")
	
	val obelisk = Array(5) {
		ResourceLocation(ModInfo.MODID, "textures/model/block/Obelisc$it.png")
	}
	
	val wings = arrayOf(
		null,
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/SALAMANDER_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/SYLPH_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/CAITSITH_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/POOKA_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/GNOME_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/LEPRECHAUN_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/SPRIGGAN_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/UNDINE_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/IMP_wing.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/ALV_wing.png")
	)
	
	val auraBird = ResourceLocation(ModInfo.MODID, "textures/model/entity/auras/auraBird.png")
	val auraDemonic = ResourceLocation(ModInfo.MODID, "textures/model/entity/auras/auraDemonic.png")
	val auraGreece = ResourceLocation(ModInfo.MODID, "textures/model/entity/auras/auraGreece.png")
	
	val wingsButterfly = ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/Butterfly.png")
	val wingsDarkPhoenix = ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/DarkPhoenix.png")
	val wingsHeavenBird = ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/HeavenBird.png")
	val wingsSprite = ResourceLocation(ModInfo.MODID, "textures/model/entity/wings/HeavenSprite.png")
	
	const val MOB = 11
	const val NPC = 12
	const val BOSS = 13
	
	val icons = arrayOf(
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/HUMAN.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/SALAMANDER.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/SYLPH.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/CAITSITH.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/POOKA.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/GNOME.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/LEPRECHAUN.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/SPRIGGAN.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/UNDINE.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/IMP.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/ALV.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/MOB.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/NPC.png"),
		ResourceLocation(ModInfo.MODID, "textures/misc/icons/BOSS.png")
	)
	
	val affinities = arrayOf(
		ResourceLocation("Omg dat's weird"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/SALAMANDER.png"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/SYLPH.png"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/CAITSITH.png"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/POOKA.png"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/GNOME.png"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/LEPRECHAUN.png"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/SPRIGGAN.png"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/UNDINE.png"),
		ResourceLocation(ModInfo.MODID, "textures/gui/spells/affinities/IMP.png")
	)
	
	val male = arrayOf(
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/Salamander.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/Sylph.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/CaitSith.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/Pooka.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/Gnome.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/Leprechaun.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/Spriggan.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/Undine.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/Imp.png")
	)
	
	val female = arrayOf(
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/Salamander.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/Sylph.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/CaitSith.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/Pooka.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/Gnome.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/Leprechaun.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/Spriggan.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/Undine.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/Imp.png")
	)
	
	val oldMale = arrayOf(
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldSalamander.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldSylph.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldCaitSith.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldPooka.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldGnome.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldLeprechaun.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldSpriggan.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldUndine.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/male/OldImp.png")
	)
	
	val oldFemale = arrayOf(
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldSalamander.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldSylph.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldCaitSith.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldPooka.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldGnome.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldLeprechaun.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldSpriggan.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldUndine.png"),
		ResourceLocation(ModInfo.MODID, "textures/model/entity/female/OldImp.png")
	)
	
	private val spells = HashMap<String, ResourceLocation>()
	
	val inventory = ResourceLocation("textures/gui/container/inventory.png")
	val widgets = ResourceLocation("textures/gui/widgets.png")
	
	fun add(name: String) =
		spells.put(name, ResourceLocation(ModInfo.MODID, "textures/gui/spells/$name.png"))
	
	fun spell(name: String) =
		spells[name] ?: affinities[0]
}